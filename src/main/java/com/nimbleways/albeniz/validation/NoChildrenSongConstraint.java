package com.nimbleways.albeniz.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NoChildrenSongValidation.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface NoChildrenSongConstraint {
    String message() default "Invalid author...";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}