package com.nimbleways.albeniz.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NoChildrenSongValidation implements
        ConstraintValidator<NoChildrenSongConstraint, String> {

    @Override
    public void initialize(NoChildrenSongConstraint constraint) {
    }

    @Override
    public boolean isValid(String author, ConstraintValidatorContext cxt) {
        return !author.contains("Chantal G.");
    }

}