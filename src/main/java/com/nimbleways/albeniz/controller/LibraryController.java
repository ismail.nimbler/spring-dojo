package com.nimbleways.albeniz.controller;

import com.nimbleways.albeniz.controller.exceptions.NotFoundException;
import com.nimbleways.albeniz.model.Tune;
import com.nimbleways.albeniz.services.LibraryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("library")
public class LibraryController {

    @Autowired
//    @Qualifier("databaseLibraryService")
    private LibraryService libraryService;

    @ApiOperation(value = "View a list of available tunes", response = Collection.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    @GetMapping("music")
    public Collection<Tune> getMusic(@ApiParam("The text to look for in the tunes' title") @RequestParam(required = false) String query) {
        return libraryService.getAll(query);
    }

    @ApiOperation(value = "View a tune", response = Tune.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved tune"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping("music/{id}")
    public ResponseEntity<Tune> getMusic(@ApiParam(required = true, value = "Tune's id") @PathVariable("id") int musicId) {
        Tune music = libraryService.getOne(musicId);
        if (music == null) throw new NotFoundException();
        return new ResponseEntity<>(music, HttpStatus.OK);
    }

    @DeleteMapping("music/{id}")
    public ResponseEntity<Void> remove(@PathVariable("id") int musicId) {
        if (libraryService.remove(musicId)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            throw new NotFoundException();
        }
    }

    @PostMapping("music")
    public ResponseEntity<Tune> addMusic(@Valid @RequestBody Tune tune) {
        boolean created = libraryService.addTune(tune);
        if (created) {
            return new ResponseEntity<>(tune, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    @PutMapping("music/{id}")
    public ResponseEntity<Tune> modifyMusic(@PathVariable("id") int musicId, @Valid @RequestBody Tune tune) {
        boolean modified = libraryService.modifyMusic(musicId, tune);
        if (modified) {
            return new ResponseEntity<>(tune, HttpStatus.OK);
        }
        throw new NotFoundException();
    }
}
