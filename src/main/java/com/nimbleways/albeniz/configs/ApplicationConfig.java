package com.nimbleways.albeniz.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * This class helps define the properties that can be loaded in the application.
 * Spring Boot will fill the values based on the property files / environment variables
 *
 * @see <a href="https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html#boot-features-external-config-conversion">https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html#boot-features-external-config-conversion</a>
 */
@Configuration
@ConfigurationProperties(prefix = "application")
@Data
public class ApplicationConfig {

    private ApiConfiguration api = new ApiConfiguration();

    @Data
    public static class ApiConfiguration {
        private Integer maxCollection = 30;
        private boolean ascending = true;
    }
}
