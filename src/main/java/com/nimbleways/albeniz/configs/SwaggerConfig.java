package com.nimbleways.albeniz.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static springfox.documentation.builders.RequestHandlerSelectors.withMethodAnnotation;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${server.servlet.context-path}")
    private String contextPath;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(
//                        any()
//                        RequestHandlerSelectors.basePackage("com.nimbleways.albeniz.controller")
                        withMethodAnnotation(GetMapping.class).or(
                                withMethodAnnotation(PostMapping.class)).or(
                                withMethodAnnotation(PatchMapping.class)).or(
                                withMethodAnnotation(PutMapping.class))
                )
                .paths(PathSelectors.ant(String.format("%s/library/**", contextPath)))
                .build()
                .apiInfo(apiInfo());

    }

    private ApiInfo apiInfo() {
        return new ApiInfo("Albeniz Library REST API", "My useful description of the API",
                "v1.0", "Terms of service",
                new Contact("ismaïl BENHALLAM", "www.nimbleways.com", "ismail.benhallam@nimbleways.com"),
                "License of API", "API license URL", Collections.emptyList());
    }
}