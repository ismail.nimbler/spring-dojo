package com.nimbleways.albeniz.services;

import com.nimbleways.albeniz.model.Tune;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

@Service
@Profile("mock")
public class MockLibraryService implements LibraryService {
    private static final Map<Integer, Tune> LIBRARY = new HashMap<>();

    static {
        LIBRARY.put(1, new Tune(1, "Thriller", "Michael J."));
        LIBRARY.put(2, new Tune(2, "Uptown Funk", "Bruno M."));
        LIBRARY.put(3, new Tune(3, "Le petit bonhomme en mousse", "Patrick S."));
    }

    @Override
    public Collection<Tune> getAll(String query) {
        return LIBRARY.values().stream()
                .sorted(Comparator.comparing(Tune::getId))
                .filter(tune -> query == null || tune.getTitle().contains(query))
                .toList();
    }

    @Override
    public Tune getOne(int id) {
        return LIBRARY.get(id);
    }

    @Override
    public boolean addTune(Tune tune) {
        return false;
    }

    @Override
    public boolean remove(int id) {
        return false;
    }

    @Override
    public boolean modifyMusic(int musicId, Tune tune) {
        return false;
    }

    @Override
    public void clean() {

    }
}
