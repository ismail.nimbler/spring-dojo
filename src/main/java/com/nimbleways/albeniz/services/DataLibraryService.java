package com.nimbleways.albeniz.services;

import com.nimbleways.albeniz.configs.ApplicationConfig;
import com.nimbleways.albeniz.model.Tune;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

@Service
@Profile("!mock")
public class DataLibraryService implements LibraryService {
    private final Map<Integer, Tune> library = new HashMap<>();

    @Autowired
    private ApplicationConfig config;

    @Override
    public Collection<Tune> getAll(String query) {
        return library.values().stream()
                .sorted(Comparator.comparing(Tune::getId))
                .filter(tune -> query == null || tune.getTitle().contains(query))
                .sorted(config.getApi().isAscending() ? Comparator.comparing(Tune::getTitle) : Comparator.comparing(Tune::getTitle).reversed())
                .limit(config.getApi().getMaxCollection())
                .toList();
    }

    @Override
    public Tune getOne(int id) {
        return library.get(id);
    }

    @Override
    public boolean addTune(Tune tune) {
        if (!library.containsKey(tune.getId())) {
            library.put(tune.getId(), tune);
            return true;
        }
        return false;
    }

    @Override
    public boolean modifyMusic(int musicId, Tune tune) {
        if (library.containsKey(musicId)) {
            Tune previousTune = library.get(musicId);
            previousTune.setAuthor(tune.getAuthor());
            previousTune.setTitle(tune.getTitle());
            library.put(musicId, previousTune);
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(int id) {
        if (library.containsKey(id)) {
            library.remove(id);
            return true;
        }
        return false;
    }

    @Override
    public void clean() {
        library.clear();
    }
}
