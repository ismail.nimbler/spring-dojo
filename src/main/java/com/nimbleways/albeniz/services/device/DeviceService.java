package com.nimbleways.albeniz.services.device;

public interface DeviceService {
    String text();
}
