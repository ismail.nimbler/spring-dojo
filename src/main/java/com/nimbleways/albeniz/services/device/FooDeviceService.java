package com.nimbleways.albeniz.services.device;

import org.springframework.stereotype.Service;

@Service("fooDeviceService")
public class FooDeviceService implements DeviceService {
    @Override
    public String text() {
        return "foo";
    }
}
