package com.nimbleways.albeniz.services.device;

import org.springframework.stereotype.Service;

@Service("barDeviceService")
public class BarDeviceService implements DeviceService {
    @Override
    public String text() {
        return "bar";
    }
}
