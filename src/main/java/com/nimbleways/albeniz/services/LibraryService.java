package com.nimbleways.albeniz.services;

import com.nimbleways.albeniz.model.Tune;

import java.util.Collection;

public interface LibraryService {
    Collection<Tune> getAll(String query);

    Tune getOne(int id);

    boolean addTune(Tune tune);

    boolean modifyMusic(int musicId, Tune tune);

    boolean remove(int id);

    void clean();
}
