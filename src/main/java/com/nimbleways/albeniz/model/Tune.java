package com.nimbleways.albeniz.model;

import com.nimbleways.albeniz.validation.NoChildrenSongConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@Data
public class Tune {
    private int id;

    @NotBlank(message = "Title is mandatory")
    private String title;

    @NoChildrenSongConstraint
    private String author;
}
