package com.nimbleways.albeniz.interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SampleInterceptor implements HandlerInterceptor {
    private static final String REQUEST_TIMER = "request.timer";

    private static Logger log = LoggerFactory.getLogger(SampleInterceptor.class);
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        log.info(String.format("Incoming request: %s", request.getPathInfo()));
        request.setAttribute(REQUEST_TIMER, System.currentTimeMillis());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        Long start = (Long) request.getAttribute(REQUEST_TIMER);
        log.info(String.format("Incoming request: %s, returned: %s in %s ms", request.getPathInfo(), response.getStatus(), getDelay(start)));

    }

    private long getDelay(Long start) {
        return System.currentTimeMillis() - start;
    }
}
