package com.nimbleways.albeniz;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AlbenizApplicationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    @LocalServerPort
    private int port;

    @Test
    void testActuatorHealthEndpoint() {
        var responseEntity = restTemplate.getForEntity(String.format("http://localhost:%d%s/health", port, contextPath),
                String.class);
        Assertions.assertThat(responseEntity
                .getStatusCode().is2xxSuccessful()).isTrue();
        Assertions.assertThat(responseEntity.getBody())
                .contains("{\"status\":\"UP\"}");
    }

}
