package com.nimbleways.albeniz.controller;

import com.nimbleways.albeniz.services.LibraryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "prod")   // NEEDED TO DEFINE IN WHICH CONTEXT THE SERVER MUST BE LAUNCHED
class Data2LibraryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LibraryService libraryService;

    @BeforeEach
    public void setUp() {
        libraryService.clean();
    }

    @Test
    void testGetMusic() throws Exception {
        mockMvc.perform(get("/library/music")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "[]"));
    }

    @Test
    void findMusic() throws Exception {
        mockMvc.perform(get("/library/music?query=i")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "[]"));
    }

    @Test
    void testGetOneTune() throws Exception {
        mockMvc.perform(get("/library/music/2")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testAddMusic() throws Exception {
        mockMvc.perform(get("/library/music")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "[]"));

        mockMvc.perform(post("/library/music")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\": 4, \"title\": \"Nougayork\", \"author\": \"Claude N.\"}"))
                .andExpect(status().isCreated());

        mockMvc.perform(get("/library/music")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "[{'id': 4, 'title': 'Nougayork', 'author': 'Claude N.'}]"));
    }

    @Test
    void testValidation() throws Exception {
        mockMvc.perform(post("/library/music")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\": 4, \"author\": \"Claude N.\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testValidationCustom() throws Exception {
        mockMvc.perform(post("/library/music")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\": 4, \"title\":\"Bécassine\", \"author\": \"Chantal G.\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testAddAndDeleteMusic() throws Exception {
        mockMvc.perform(get("/library/music")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "[]"));

        mockMvc.perform(post("/library/music")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\": 4, \"title\": \"Nougayork\", \"author\": \"Claude N.\"}"))
                .andExpect(status().isCreated());

        mockMvc.perform(post("/library/music")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\": 4, \"title\": \"Nougayork\", \"author\": \"Claude N.\"}"))
                .andExpect(status().isConflict());

        mockMvc.perform(get("/library/music")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "[{'id': 4, 'title': 'Nougayork', 'author': 'Claude N.'}]"));

        mockMvc.perform(get("/library/music/4")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{'id': 4, 'title': 'Nougayork', 'author': 'Claude N.'}"));

        mockMvc.perform(put("/library/music/4")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\": 4, \"title\": \"Le jazz et la java\", \"author\": \"Claude N.\"}"))
                .andExpect(status().isOk());

        mockMvc.perform(put("/library/music/5")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\": 5, \"title\": \"Thunderstruck\", \"author\": \"AC DC\"}"))
                .andExpect(status().isNotFound());

        mockMvc.perform(get("/library/music/4")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{'id': 4, 'title': 'Le jazz et la java', 'author': 'Claude N.'}"));

        mockMvc.perform(delete("/library/music/4")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(delete("/library/music/5")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());


        mockMvc.perform(get("/library/music")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "[]"));
    }
}
