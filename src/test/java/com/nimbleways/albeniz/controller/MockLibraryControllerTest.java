package com.nimbleways.albeniz.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

//@WebMvcTest
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("mock")
class MockLibraryControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void getMusic() throws Exception {
        String result = """
                [
                  {
                    "title": "Thriller",
                    "author": "Michael J."
                  },
                  {
                    "title": "Uptown Funk",
                    "author": "Bruno M."
                  },
                  {
                    "title": "Le petit bonhomme en mousse",
                    "author": "Patrick S."
                  }
                ]""";
        mockMvc.perform(MockMvcRequestBuilders.get("/library/music"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(result));
    }

    @Test
    void getMusicWithQuery() throws Exception {
        String result = """
                [
                  {
                    "id": 2,
                    "title": "Uptown Funk",
                    "author": "Bruno M."
                  },
                  {
                    "id": 3,
                    "title": "Le petit bonhomme en mousse",
                    "author": "Patrick S."
                  }
                ]""";
        mockMvc.perform(MockMvcRequestBuilders.get("/library/music?query=u"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(result));
    }

    @Test
    void getMusicById() throws Exception {
        String result = """
                  {
                    "title": "Uptown Funk",
                    "author": "Bruno M.",
                    "id": 2
                  }
                """;
        mockMvc.perform(MockMvcRequestBuilders.get("/library/music/2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(result));
    }
}