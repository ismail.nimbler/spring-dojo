package com.nimbleways.albeniz.controller;

import com.nimbleways.albeniz.services.LibraryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

//@WebMvcTest
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("prod")
class DateLibraryControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LibraryService libraryService;

    @BeforeEach
    void prepare(){
        libraryService.clean();
    }

    @Test
    void add() throws Exception {
        // The library is empty
        mockMvc.perform(MockMvcRequestBuilders.get("/library/music"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("[]"));

        // Add new music to the library
        String requestBody = """
                  {
                    "id": 1,
                    "title": "Arabi",
                    "author": "Inkonnu"
                  },
                """;
        mockMvc.perform(MockMvcRequestBuilders.post("/library/music")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isCreated());

        // Test if the music is saved correctly
        mockMvc.perform(MockMvcRequestBuilders.get("/library/music"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("""
                        [
                            {
                                "id": 1,
                                "title": "Arabi",
                                "author": "Inkonnu"
                            }
                        ]
                        """));
    }

    @Test
    void addNonValid() throws Exception {
        String requestBody = """
                  {
                    "id": 1516,
                    "title": "",
                    "author": "Inkonnu"
                  },
                """;
        mockMvc.perform(MockMvcRequestBuilders.post("/library/music")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());


        requestBody = """
                  {
                    "id": 1785,
                    "title": "Zahri",
                    "author": "Chantal G."
                  },
                """;
        mockMvc.perform(MockMvcRequestBuilders.post("/library/music")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
//                .andExpect(MockMvcResultMatchers.content().string("Invalid author..."))
        ;
    }

    @Test
    void delete() throws Exception {
        // The library is empty
        mockMvc.perform(MockMvcRequestBuilders.get("/library/music"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("[]"));

        // Add new music to the library
        String requestBody = """
                  {
                    "id": -54,
                    "title": "Arabi",
                    "author": "Inkonnu"
                  },
                """;
        mockMvc.perform(MockMvcRequestBuilders.post("/library/music")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isCreated());

        // Test if the music is saved correctly
        mockMvc.perform(MockMvcRequestBuilders.get("/library/music"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("""
                        [
                            {
                                "id": -54,
                                "title": "Arabi",
                                "author": "Inkonnu"
                            }
                        ]
                        """));

        // Delete the saved music
        mockMvc.perform(MockMvcRequestBuilders.delete("/library/music/-54"))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // The library is empty, again
        mockMvc.perform(MockMvcRequestBuilders.get("/library/music"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("[]"));

    }
}